﻿using System;
using System.IO;
using System.Threading;

public class FileLocker
{
    public static void LockFile(string filePath, int durationMilliseconds)
    {
        using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Write, FileShare.None))
        {
            Console.WriteLine($"Locked file {filePath} for {durationMilliseconds} milliseconds.");
            Thread.Sleep(durationMilliseconds);
        }
        
        Console.WriteLine($"Released file {filePath}.");
    }

    public static void Main()
    {
        string filePath = @"C:\FTG\rollover\MyThingBlah\test.txt";
        int durationMilliseconds = 10000; // Lock the file for 10 seconds

        LockFile(filePath, durationMilliseconds);
    }
}

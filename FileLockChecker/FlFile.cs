namespace FileLockChecker;

public class FlFile
{
    public string FilePath { get; set; }

    public int LockedTimes { get; set; } = 0;
    // Total locked time in milliseconds
    public int TotalLockedTime { get; set; } = 0;
    public DateTime? LockedSince { get; set; } = null;
    public List<LockInfo> LockInfos { get; set; } = new();
    public DateTime LastChecked { get; set; } = DateTime.MinValue;
    public bool IsLocked => LockedSince.HasValue; 

    public FlFile(string filePath)
    {
        this.FilePath = filePath;
    }
}
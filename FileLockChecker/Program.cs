﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using NtApiDotNet;

namespace FileLockChecker;

public static class Program
{
    public static LockInfo? GetLockingProcessName(string filePath)
    {
        string toolPath = @"C:\FTG\handle.exe";
        string arguments = $"-nobanner \"{filePath}\" -accepteula";

        Process process = new Process();
        process.StartInfo.FileName = toolPath;
        process.StartInfo.Arguments = arguments;
        process.StartInfo.UseShellExecute = false;
        process.StartInfo.RedirectStandardOutput = true;
        process.StartInfo.CreateNoWindow = true;

        process.Start();
        string output = process.StandardOutput.ReadToEnd();
        process.WaitForExit();

        // Parse output to find the process name and return it.
        string[] lines = output.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        foreach (string line in lines)
        {
            //Console.WriteLine($"Output line: {line}");
            if (line.Contains("pid:"))
            {
                var handleInfo = new LockInfo(line);
                return handleInfo;
            }
        }

        return null;
    }
    
    
    private static SemaphoreSlim _semaphore = new SemaphoreSlim(3); // Allows 3 simultaneous invocations
    private static ConcurrentQueue<string> _fileScanQueue = new ();
    private static ConcurrentDictionary<string, FlFile> _filesBeingProcessed = new ();
    private static CancellationTokenSource _cts = new CancellationTokenSource();
    private static List<LockInfo> _lockInfos = new ();
    private static string folderPath = @"C:\FTG\rollover\MyThingBlah";
    
    static void CheckFiles(CancellationToken token)
    {
        while (true)
        {
            if (token.IsCancellationRequested)
            {
                Console.WriteLine("Task cancellation detected.");
                break; // Exit the loop and end the task
            }
            if (_fileScanQueue.TryDequeue(out var file))
            {
                FlFile? targetFile = _filesBeingProcessed[file];
                if (targetFile == null)
                {
                    throw new Exception($"Blah");
                }
                
                if ((DateTime.Now - targetFile.LastChecked).TotalMilliseconds < 500)
                {
                    // If it hasn't been 500ms since the last check, requeue the file and skip to the next iteration
                    _fileScanQueue.Enqueue(file);
                    continue;
                }

                targetFile.LastChecked = DateTime.Now;
                _semaphore.Wait();

                try
                {
                    var lockingProcess = GetLockingProcessName(file);
                    if (lockingProcess != null)
                    {
                        if (targetFile.LockedSince == null)
                        {
                            targetFile.LockedSince = DateTime.Now;
                            targetFile.LockedTimes += 1;
                            if (!targetFile.LockInfos.Any(x => x.ProcessId == lockingProcess.ProcessId))
                            {
                               targetFile.LockInfos.Add(lockingProcess); 
                            }
                        }
                        //Console.WriteLine($"File {file} is locked by process {lockingProcess.ProcessName}.");
                    }
                    else
                    {
                        if (targetFile.LockedSince != null)
                        {
                            targetFile.TotalLockedTime += (DateTime.Now - targetFile.LockedSince).Value.Seconds;
                            targetFile.LockedSince = null;
                        }
                    }

                    _filesBeingProcessed[file] = targetFile;
                    _fileScanQueue.Enqueue(file);
                    // Process the result as needed...
                }
                finally
                {
                    _semaphore.Release();
                }
            }
        }
    }
    
    static void ListenForUserCancel()
    {
        while (true)
        {
            var key = Console.ReadKey(intercept: true).Key;
            if (key == ConsoleKey.Q || key == ConsoleKey.Escape)
            {
                Console.WriteLine("Cancellation requested by user.");
                _cts.Cancel();
                break;
            }
        }
    }
    

    public static void Main()
    {
        Console.WriteLine("Press 'q' or 'esc' to exit.");
        var tasks = new List<Task>();
        // Spawn a separate task to listen for user input
        Task.Run(() => ListenForUserCancel());

        for (int i = 0; i < 10; i++)
        {
            tasks.Add(Task.Run(() =>
            {
                CheckFiles(_cts.Token);
            }));
        }
            
        while (!_cts.IsCancellationRequested) // Endless loop
        {
            PopulateFilesQueue();
            
            foreach(var fi in _filesBeingProcessed.Values)
            {
                // if (fi.IsLocked)
                // {
                //     Console.WriteLine($"File {fi.FilePath} has been locked since {fi.LockedSince} Total ({fi.TotalLockedTime})");
                // }
                // else
                // {
                //     //Console.WriteLine($"File {fi.FilePath} is not locked.");    
                // }
                Console.WriteLine($"File {fi.FilePath} is locked? {fi.IsLocked}");    
                
                Console.WriteLine($"-----Locks-----");
                foreach(var item in fi.LockInfos)
                {
                    Console.WriteLine($"\t {item.ProcessName} (PID {item.ProcessId}) has locked {fi.FilePath}");
                }
                Console.WriteLine($"-----End Locks-----");
            }

            // Sleep for a bit before checking again
            // Adjust this delay as needed
            Thread.Sleep(1000); 
            //Console.Clear();
        }
        Console.ReadLine();

    }
    
    static void PopulateFilesQueue()
    {
        foreach (var filePath in System.IO.Directory.GetFiles(folderPath))
        {
            if (!_filesBeingProcessed.ContainsKey(filePath))
            {
                Console.WriteLine($"Queued file {filePath} for processing.");
                _filesBeingProcessed[filePath] = new FlFile(filePath);
                _fileScanQueue.Enqueue(filePath);
            }
        }
        //Console.WriteLine($"Finished populating file queue");
    }
}
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace FileLockChecker;

public class LockInfo
{
    public string ProcessName { get; set; }
    public int ProcessId { get; set; }
    public string HandleType { get; set; }
    public int HandleNumber { get; set; }

    public LockInfo(string outputLine)
    {
        // var parts = outputLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        //
        // if (parts.Length < 8)
        // {
        //     throw new ArgumentException("Invalid output format.");
        // }
        //
        // this.ProcessName = parts[0];
        // this.ProcessId = int.Parse(parts[2]);
        // this.HandleType = parts[4];
        // this.HandleNumber = int.Parse(parts[6].TrimEnd(':'));
        // this.FilePath = string.Join(" ", parts, 7, parts.Length - 7);
        
        //var regex = new Regex(@"^(?<processName>\S+)\s+pid:\s+(?<pid>\d+)\s+type:\s+(?<type>\S+)\s+(?<handleNumber>\d+):\s+(?<filePath>.+)$");
        var regex = new Regex(@"^(?<processName>\S+)\s+pid:\s+(?<pid>\d+)\s+type:\s+(?<type>\S+)\s+(?<handleNumber>[0-9a-fA-F]+):\s+(?<filePath>.+)$");


        var match = regex.Match(outputLine);

        if (!match.Success)
        {
            throw new ArgumentException($"Invalid output format. Line: '{outputLine}'");
        }

        this.ProcessName = match.Groups["processName"].Value;
        this.ProcessId = int.Parse(match.Groups["pid"].Value);
        this.HandleType = match.Groups["type"].Value;
        //this.HandleNumber = int.Parse(match.Groups["handleNumber"].Value);
        this.HandleNumber = int.Parse(match.Groups["handleNumber"].Value, System.Globalization.NumberStyles.HexNumber);

        //this.FilePath = match.Groups["filePath"].Value;

    }

    public override string ToString()
    {
        return JsonConvert.SerializeObject(this);
    }
}